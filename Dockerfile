FROM node:16-alpine3.11

RUN npm install -g cordova ionic
ADD . /usr/local/apps

WORKDIR /usr/local/apps

EXPOSE 3000
ENTRYPOINT ["npm"]