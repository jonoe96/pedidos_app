import axios from 'axios'
import { IFood } from './food.interface'


export const useFoodService = () => {
    
    const getFoodList = async (): Promise<Array<IFood>> => {
        return []
    }

    return { getFoodList }
}