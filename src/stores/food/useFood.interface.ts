import { IFood } from "./food.interface";

export interface IFoodStore {
    foodList: Array<IFood>
    getFoodList: () => Promise<IFoodStore['foodList']>
}

export type TUseFoodStore = () => IFoodStore