import React from 'react'
import { IFoodStore, TUseFoodStore } from "./useFood.interface";
import { useFoodService } from './useFood.service';

export const useFoodStore: TUseFoodStore =  () => {

    const foodService = useFoodService()

    const [foodList, setFoodList] = React.useState<IFoodStore['foodList']>([])

    const getFoodList: IFoodStore['getFoodList'] = async () => {
        const response = await foodService.getFoodList()
        setFoodList(response)
        return response
    }

    return {foodList, getFoodList}
}