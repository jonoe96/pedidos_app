export interface IFood {
    id: number
    title: string
    text: string
}
