import React from 'react'
import { useParams } from 'react-router'
import { IFoodStore } from '../stores/food/useFood.interface'

interface IFoodListPageProps {
    foodStore: IFoodStore
}

export const FoodListPage: React.FC<IFoodListPageProps> = (props) => {
    const param = useParams<{ id: string }>()
    // param.id en el caso que la ruta del router defina un parametro '/:id'
    return (
        <h1>food list</h1>
    )
}