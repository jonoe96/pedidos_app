import React from 'react'
import { Nav, NavLogoImage, NavMenuButton } from '../elements/nav.element'


interface INavBarComponentProps {
    onClickMenuButton: () => void
}

export const NavBarComponent: React.FC<INavBarComponentProps> = (props) => {
    return (
        <Nav>
            <NavMenuButton onClick={props.onClickMenuButton}>Menu</NavMenuButton>
            <NavLogoImage src="https://www.seekpng.com/png/full/124-1249276_comida-icono-png-food-icon-png-color.png"/>
        </Nav>
    )
} 