import React from 'react'
import { CloseMenu, Menu, MenuHeader, MenuItem, MenuList, MenuLink } from '../elements/menu.element'

interface IMenuComponentProps {
    isOpen: boolean
    onClose: () => void
}

export const MenuComponent: React.FC<IMenuComponentProps> = (props) => {


    return (
        <Menu isOpen={props.isOpen}>
            <MenuHeader>
                <CloseMenu onClick={props.onClose}>X</CloseMenu>
            </MenuHeader>
            <MenuList>
                <MenuItem onClick={props.onClose}>
                    <MenuLink url={`/`} text="Lista de Comidas"/>
                </MenuItem>
                <MenuItem onClick={props.onClose}>
                    <MenuLink url={`/other`} text="Otro" />
                </MenuItem>
            </MenuList>
        </Menu>
    )
}