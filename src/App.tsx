import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { MenuComponent } from './components/Menu.component';
import { NavBarComponent } from './components/NavBar.component';
import { All } from './elements/all';
import { Page } from './elements/page.element';
import { FoodListPage } from './pages/FoodList.page';
import { useFoodStore } from './stores/food/useFood.store';

const App = () => {


  const foodStore = useFoodStore()

  const [isOpenMenu, setIsOpenMenu] = React.useState<boolean>(false)
  const onClickMenuButtonHandler = () => setIsOpenMenu(true)
  const onCloseMenuHandler = () => {
    console.log('cerramos')
    setIsOpenMenu(false)
  }
  return (
    <All>
      <Router>
        <NavBarComponent onClickMenuButton={onClickMenuButtonHandler} />
        <MenuComponent isOpen={isOpenMenu} onClose={onCloseMenuHandler} />
        <Page>
          <Switch>
            <Route exact path="/">
              <FoodListPage foodStore={foodStore} />
            </Route>
            <Route exact path="/other">
              <h1>other</h1>
            </Route>
            <Redirect to="/" />
          </Switch>
        </Page>
      </Router>
    </All>
  )
}

export default App;
