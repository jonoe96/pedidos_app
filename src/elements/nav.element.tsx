import React from 'react'
import styled from 'styled-components'

export const Nav = styled.nav `
    background: red;
    position: fixed;
    width: 100%;
    top: 0;
    left: 0;
    display: flex;
    flex-direction: row;

`

export const NavLogoImage = styled.img `
    width: 20px;
    height: 20px;
`

export const NavMenuButton = styled.button`
    border: none;
    background: none;
    color: white;
    font-size: 16px;
    cursor: pointer;
`