import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

export const Menu = styled.div<{isOpen: boolean}>`
    position: fixed;
    top: 0;
    left: ${props=>props.isOpen ? 0 : -200}px;
    transition: left 0.1s ease-in;
    width: 190px;
    height: 100vh;
    background: orange;
    display: flex;
    flex-direction: column;
`

export const MenuHeader = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: end;
`

export const CloseMenu = styled.button`
    background: none;
    color: white;
    border: none;
    font-size: 16px;
    cursor: pointer;
`

export const MenuList = styled.ul`
    padding: 0px;
    margin: 0px;
    width: 100%;
    list-style: none;
    display: flex;
    flex-direction: column;
`

export const MenuItem = styled.li`
    width: 100%;
    padding: 0px;
    margin: 0px;
    display: flex;
    background: pink;
`



export const MenuLink = (props: {url: string, text: string})=> {
    return <Link to={props.url} style={{ width:'100%', padding: '20px', textDecoration: 'none', color: 'white' }}>{props.text}</Link>
}


