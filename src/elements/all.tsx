import React from 'react'
import styled from 'styled-components'

document.getElementsByTagName('body')[0].style.padding = '0px'

export const All = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    font-family: Arial;
    background:#eee;
    color: #334;
    box-sizing: border-box;
`