#!/usr/bin/env bash

echo "Running pedidos_app..."
docker run -p 3000:3000 -v $(pwd):/usr/local/apps --rm ionic run start
if [ "$?" -gt "0" ]; then
    echo "Error running pedidos_app"
fi